<?php

namespace App\Http\Controllers;

use App\Models\AssignUser;
use Illuminate\Http\Request;

class AssignUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'roles' => 'string|required',
        ]);

        $assignedViewer = AssignUser::where(['project_id' => $request->project_id, 'roles' => 'viewer'])->count();
        $assignedAuthor = AssignUser::where(['project_id' => $request->project_id, 'roles' => 'author'])->count();

        if ($assignedViewer <= 5 || $assignedAuthor <= 2) {
            AssignUser::create([
                'project_id' => $request->project_id,
                'user_id' => $request->user_id,
                'roles' => $request->roles,
            ]);
            return redirect('/project/' . $request->project_id)->with('success', 'successfully assigned as ' . $request->roles . '!');
        } else {
            return redirect('/project/' . $request->project_id)->with('error', "you've reached the number users to assign to this project");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
