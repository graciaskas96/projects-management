<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_users = User::where('id', '!=', Auth::id())->orderBy('updated_at', 'desc')->get();

        return view('admin.users', [
            'all_users' => $all_users,
            'title' => 'Users',
            'desc' => 'This is meta description for all Users',
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'gender' => ['required', 'string', 'max:255'],
            'title' => ['required', 'string', 'max:255'],
            'role' => ['required', 'string', 'max:255'],
            'password' => ['required', 'confirmed'],
        ]);

        if ($request->hasFile('avatar')) {
            $avatar = Storage::disk('local')->put('avatars', $request->file('avatar'));
            $user = User::create([
                'avatar' => $avatar,
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'phone' => $request->phone,
                'title' => $request->title,
                'role' => $request->role,
                'password' => Hash::make($request->password),
            ]);
        } else {
            $user = User::create([
                'avatar' => 'N/A',
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'phone' => $request->phone,
                'title' => $request->title,
                'role' => $request->role,
                'password' => Hash::make($request->password),
            ]);
        }

        event(new Registered($user));

        Auth::login($user);

        return redirect('register')->with('success', 'user successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_user = User::findOrFail($id);

        return view('admin.user_form', [
            'edit_user' => $edit_user,
            'title' => ucfirst($edit_user->name),
            'desc' => 'This is meta description for User Edit',
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'gender' => ['required', 'string', 'max:255'],
            'title' => ['required', 'string', 'max:255'],
            'role' => ['required', 'string', 'max:255'],
        ]);

        if ($request->hasFile('avatar')) {
            $avatar = Storage::disk('local')->put('avatars', $request->file('avatar'));
            $user = User::findOrFail($id)->update([
                'avatar' => $avatar,
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'phone' => $request->phone,
                'title' => $request->title,
                'role' => $request->role,
            ]);
        } else {
            $user = User::findOrFail($id)->update([
                'avatar' => 'N/A',
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'phone' => $request->phone,
                'title' => $request->title,
                'role' => $request->role,
            ]);
        }

        event(new Registered($user));

// Auth::login($user);

        return redirect('register')->with('success', 'user successfully updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::withTrashed()
            ->where('id', $id)
            ->get();
        redirect('admin/users');
    }
}
