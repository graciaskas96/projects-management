<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user_id = random_int(1,10);
        return [
            'name' => $this->faker->sentence(),
            'link' => $this->faker->sentence(),
            'description' => $this->faker->paragraph(),
            'image_url' => "NULL",
            'user_id' =>  $user_id,
            'category_id' =>  $user_id,
            'status' => "active",
            'link' => "www.example.com/project/".$user_id
        ];
    }
}
