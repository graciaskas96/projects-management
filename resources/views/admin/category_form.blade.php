<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Settings') }}
        </h2>
    </x-slot>

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-red-500 text-white absolute" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Error</p>
                    <p class="text-sm">{{$error}}</p>
                </div>
            </div>
        </div>
        @endforeach
    @endif



    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="bg-blue-500 overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="pt-8 pb-4" style="height: 200px !important;">
                    <h4 class="bg-white border rounded mt-2 p-2 ml-2">All Categories</h4>
                        <div class="pr-2" style="height: 200px !important;">
                            <ul class="row" style="height: 200px !important;">
                                @foreach($all_categories as $category)
                                    <div class="col-lg-6">
                                        <li class="box__user graph">
                                            <div class="box__description">
                                                <a href="{{route('categories.edit', $category->id)}}" class="">{{$category->title}}</a>
                                                <p class="overflow-ellipsis w-full font-thin capitalize text-gray-200 text-xs">{{$category->desc}}</p>
                                            </div>
                                            <div class="box__image">
                                                <a href="{{route('categories.edit', $category->id)}}" class="">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h- w-25" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                                    </svg>
                                                </a>
                                                <a onclick="return confirm('etes-vous sur de vouloir supprimer?')" href="{{route('categories.destroy', $category->id)}}" class="text-danger">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-25" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M6 18L18 6M6 6l12 12" />
                                                    </svg>
                                                </a>
                                            </div>
                                        </li>
                                    </div>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="bg-white border rounded m-2 p-4">

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{session('success')}}
                        </div>
                    @endif

                    <div class="border-bottom">
                        <p>Create new category</p>
                    </div>
                    @if(request()->routeIs('categories.index'))
                        <form action="{{route('categories.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('post')
                            <div class="form-input">
                                <label class="">Title</label>
                                <input class="form-control" type="text" placeholder="Title..." name="title" />
                            </div>

                            <div class="form-input">
                                <label class="">Description</label>
                                <textarea class="form-control resize-none" name="desc" cols="30" rows="4" placeholder="Description..."></textarea>
                            </div>

                            <div class="mt-2 border-bottom mb-2">
                                <label for="file-upload" class="d-flex justify-content-between">
                                    <!-- <svg class="mx-auto h-12 w-12 text-gray-400 w-25" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                                        <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg> -->
                                    <span class="btn btn-sm btn-primary">Upload image</span>
                                    <input id="file-upload" name="post_img" type="file" class="sr-only" accept="image/jpeg, image/png">
                                </label>
                            </div>

                            <div class="">
                                <button class="btn btn-sm btn-primary" type="submit">Save</button>
                                <a href="{{route('categories.index')}}" class="btn btn-sm btn-danger" type="reset">Clear</a>
                            </div>
                        </form>
                            @else
                                <form action="{{route('categories.update', $edit_category->id)}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="form-input">
                                        <label class="block text-sm font-medium text-gray-700">Title</label>
                                        <input class="form-control" type="text" placeholder="Title..." name="title" value="{{$edit_category->title}}" />
                                    </div>

                                    <div class="mb-2">
                                        <label class="block text-sm font-medium text-gray-700">Description</label>
                                        <textarea class="form-control resize-none" name="desc" cols="30" rows="4" placeholder="Description...">{{$edit_category->desc}}</textarea>
                                    </div>

                                    <div class="mb-2 border-bottom">
                                        <div class="flex text-sm text-gray-600">
                                            <label for="file-upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                <span class="btn btn-sm btn-primary">Upload</span>
                                                <input id="file-upload" name="post_img" type="file" class="sr-only" accept="image/jpeg, image/png">
                                            </label>
                                        </div>
                                    </div>

                                    <div class="flex space-x-3 mb-4 text-sm font-medium">
                                        <div class="flex-auto flex space-x-3">
                                            <button class="btn btn-sm btn-primary" type="submit">Update</button>
                                            <a href="{{route('categories.index')}}" class="btn btn-sm btn-danger" type="reset">Clear</a>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
