<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Categories', 'Projects'],
      @foreach ($categories as $category)
      [ "{{ $category->title }}", {{ $category->projects->count() }} ],
      @endforeach
    ]);

    var options = {
      title: 'Projects by Category',
      is3D : true
    };

    var chart = new google.visualization.PieChart(document.getElementById('pie-chart'));

    chart.draw(data, options);
  }
</script>

<div class="container">

	<div id="pie-chart" class="w-full" style="height: 500px;" ></div>

</div>
