<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="container-fluid container_ p-4">

        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="bg-white m-1 border rounded">
                    <div class="d-flex justify-content-between pt-2 pr-2 pl-2 border-bottom">
                        <p class="p-box">Today Users</p>
                        <p>{{$new_users->count()}}</p>
                    </div>
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-25" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                        </svg>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="bg-white m-1 border rounded">
                    <div class="d-flex justify-content-between pt-2 pr-2 pl-2 border-bottom">
                        <p class="badge bg-warning text-white">{{$users->count()}}</p>
                        <p class="p-box">All Users</p>
                    </div>
                    <div class="text-warning">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-25" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="bg-white border rounded text-primary m-1">
                    <div class="d-flex justify-content-between pt-2 pr-2 pl-2 border-bottom">
                        <p>Today Projects</p>
                        <p class="badge bg-primary text-white">{{$new_projects->count()}}</p>
                    </div>
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-25" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M8 13v-1m4 1v-3m4 3V8M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z" />
                        </svg>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="bg-white border rounded text-primary m-1">
                    <div class="d-flex justify-content-between pt-2 pr-2 pl-2 border-bottom">
                        <p>All Projects</p>
                        <p class="badge bg-primary text-white">{{$projects->count()}}</p>
                    </div>
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-25" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M7 12l3-3 3 3 4-4M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z" />
                        </svg>
                    </div>
                </div>
            </div>

        </div>

        
        <div class="mt-3">
            <div class="row">

                <div class="col-lg-6">
                    <div class="bg-white m-1 border rounded" style="height: 300px;">
                        @include('charts.google-bar-chart')
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="bg-white m-1 border rounded">
                         @include('charts.google-pie-chart')
                    </div>
                </div>

            </div>
        </div>
    
        
    </div>
</x-app-layout>
