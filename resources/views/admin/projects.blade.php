<x-app-layout>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects') }}
        </h2>
    </x-slot>


    @if(Session::has('success'))
        <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md absolute" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Success</p>
                    <p class="text-sm">{{session('success')}}</p>
                </div>
            </div>
        </div>
    @endif

    <div class="container">

        <div class="content-header mt-2 mb-4 p-2">
            <div class="row">
                <div class="col-lg-6">
                    <p class="p-box">Projects</p>
                    <div class="buttons">
                        <button class="btn btn-primary bnt-sm">Create</button>
                        <button class="btn btn-sm bg-gray">Import</button>
                    </div>
                </div>
                <div class="col-lg-6"></div>
            </div>
        </div>

        <div class="bg-white">
            <table class="table table-sm bg-white border">
                <thead>
                    <tr>
                        <th class="">Project</th>
                        <th class="">Status</th>
                        <th class="">Owner</th>
                        <th class="">Members</th>
                        <th class="">Completion </th>
                        <th class=""></th>
                    </tr>
                </thead>

                <tbody class="divide-y">
                    @foreach($all_projects as $project)
                        <tr>
                            <td class="">
                                @if($project->image_url == 'N/A')
                                    <small class="h-12 w-12 font-thin text-xs text-blue-500 bg-white rounded-full border flex justify-center items-center ring-2 ring-white">
                                        no image
                                    </small>
                                @else
                                    <img src="{{Storage::url($project->image_url)}}" class="rounded border">
                                @endif
                                {{$project->name}}
                            </td>
                            <td class="">
                                <i class="fas fa-circle text-yellow-500 mr-2"></i>{{$project->status}}</td>
                            <td class="">
                                @foreach($all_users as $user)
                                    @if($user->id == $project->user_id)
                                        {{$user->name}}
                                    @endif
                                @endforeach
                            </td>
                                            
                            <td class="">
                                <div class="flex">
                                    <?php
                                        $assigned_users = DB::table('assign_users')
                                        ->join('users', 'assign_users.user_id', '=', 'users.id')
                                        ->where('project_id', '=', $project->id)
                                        ->select('users.*')->get();
                                    ?>
                                    @foreach($assigned_users as $user)
                                        <img src="{{Storage::url($user->avatar)}}" alt="..." class="w-6 h-6 rounded-full border-2 border-blueGray-50 shadow -ml-4">
                                    @endforeach
                                </div>
                            </td>

                            <td class="">
                                <div class="flex items-center">
                                    <span class="mr-2">60%</span>
                                    <div class="relative w-full">
                                        <div class="overflow-hidden h-2 text-xs flex rounded bg-white">
                                            <div style="width: 60%" class="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-yellow-500">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</x-app-layout>
