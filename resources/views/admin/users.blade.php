<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects') }}
        </h2>
    </x-slot>



    <div class="container-fluid">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <section class="relative bg-blueGray-50">
                <div class="bg-white d-block pt-1 pl-2 rounded mt-2 border mb-2">
                    <h5>List users</h5>
                </div>
                <div class="w-100 ">
                    <table class="table table-sm bg-white rounded">
                        <thead>
                            <tr>
                                <th class="">Name</th>
                                <th class="">Email</th>
                                <th class="">Phone</th>
                                <th class="">Status</th>
                                <th class="">Role</th>
                                <th class="">Projects</th>
                                <th class=""></th>
                            </tr>
                        </thead>

                        <tbody class="divide-y">
                            @foreach($all_users as $user)
                                <tr>
                                    <td>
                                        @if($user->avatar == 'N/A')
                                            <small class="h-12 w-12 font-thin text-xs text-blue-500 bg-white rounded-full border flex justify-center items-center">
                                                no image
                                            </small>
                                        @else
                                            <img src="{{Storage::url($user->avatar)}}" class="h-12 w-12 bg-white rounded border" alt="...">
                                        @endif

                                        {{$user->name}} 
                                    </td>
                                            <td class="">
                                                {{$user->email}}
                                            </td>
                                            <td class="capitalize">
                                                {{$user->phone}}
                                            </td>
                                            <td class="">
                                                <i class="fas fa-circle text-yellow-500 mr-2"></i>{{$user->status}}</td>
                                            <td class="capitalize">
                                                {{$user->role}}
                                            </td>
                                            <td class="">
                                                10
                                            </td>
                                            <td>
                                                <a href="{{route('users.destroy', $user->id)}}" 
                                                    class="status bg-danger">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>

</x-app-layout>
