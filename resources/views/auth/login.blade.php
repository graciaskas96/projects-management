<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" />
        <!-- Styles -->
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
    </head>
    <body class="font-sans antialiased">
        <div class="login-container">
            <div class="s-main border">
                <div class="s-container">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <div class="head text-center" style="padding: 0px;">
                        <h4 class="hi-primary"> 
                        <img src="/img/logo_prouser.png" alt="" srcset="" width="35px" style="margin-top: -5px;"> ProUser | MS </h4>
                    </div>
                    <form method="POST" action="{{ route('login') }}" class="form" id="form-login">
                        @csrf
                        <!-- Email Address -->
                        <div class="form-e">
                            <span class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="22" height="22"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 17c3.662 0 6.865 1.575 8.607 3.925l-1.842.871C17.347 20.116 14.847 19 12 19c-2.847 0-5.347 1.116-6.765 2.796l-1.841-.872C5.136 18.574 8.338 17 12 17zm0-15a5 5 0 0 1 5 5v3a5 5 0 0 1-4.783 4.995L12 15a5 5 0 0 1-5-5V7a5 5 0 0 1 4.783-4.995L12 2zm0 2a3 3 0 0 0-2.995 2.824L9 7v3a3 3 0 0 0 5.995.176L15 10V7a3 3 0 0 0-3-3z" fill="rgba(24,118,211,1)"/></svg>
                            </span>
                            <input id="email" type="email"  name="email"  value="{{old('email')}}"  placeholder="Email" required  autofocus />
                        </div>
                       
                        <!-- Password -->
                        <div class="form-e">
                            <span class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="22" height="22"><path fill="none" d="M0 0h24v24H0z"/><path d="M19 10h1a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V11a1 1 0 0 1 1-1h1V9a7 7 0 1 1 14 0v1zM5 12v8h14v-8H5zm6 2h2v4h-2v-4zm6-4V9A5 5 0 0 0 7 9v1h10z" fill="rgba(24,118,211,1)"/></svg>
                            </span>
                            <input id="password"  placeholder="Password"  name="password" type="password"  required autocomplete="current-password"     /> 
                        </div>

                        <div class="d-flex justify-content-between">
                            <button type="submit"  class="btn btn-primary">  {{ __('Log in') }} </button>
                            @if (Route::has('password.request'))
                                <a class="text-right" href="{{ route('register') }}">
                                    {{ __('Sign up ?') }}
                                </a>
                            @endif
                        </div>
                       

                       
                      
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
