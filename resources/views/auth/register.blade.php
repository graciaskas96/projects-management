
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" />
        <!-- Styles -->
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
    </head>
    <body class="font-sans antialiased">
        <div class="login-container">
            <div class="s-main border rounded">
                <div class="s-container rounded">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" class="form">
                        @csrf
                        <div>
                            <div class="mt-2 d-flex justify-content-between">
                                <span>
                                    <img  id="preview-image-before-upload" src="" alt="" :style="width:50px; height : 50px; border-radius 100px;">
                                </span>
                                <label for="file-upload" class="ml-4 relative cursor-pointer ">
                                    <span class="btn btn-primary btn-sm">Image</span>
                                    <input id="file-upload" name="avatar" type="file" class="sr-only">
                                </label>
                            </div>
                        </div>

                    <div class="form-e">
                        <span class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="22" height="22"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 17c3.662 0 6.865 1.575 8.607 3.925l-1.842.871C17.347 20.116 14.847 19 12 19c-2.847 0-5.347 1.116-6.765 2.796l-1.841-.872C5.136 18.574 8.338 17 12 17zm0-15a5 5 0 0 1 5 5v3a5 5 0 0 1-4.783 4.995L12 15a5 5 0 0 1-5-5V7a5 5 0 0 1 4.783-4.995L12 2zm0 2a3 3 0 0 0-2.995 2.824L9 7v3a3 3 0 0 0 5.995.176L15 10V7a3 3 0 0 0-3-3z" fill="rgba(24,118,211,1)"/></svg>
                        </span>
                        <input id="name" class="mt-1 " type="text" name="name" value="{{old('name')}}" required autofocus placeholder="Name"/>
                    </div>

                    <!-- Email Address -->
                    <div class="form-e">
                        <span class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M22 20.007a1 1 0 0 1-.992.993H2.992A.993.993 0 0 1 2 20.007V19h18V7.3l-8 7.2-10-9V4a1 1 0 0 1 1-1h18a1 1 0 0 1 1 1v16.007zM4.434 5L12 11.81 19.566 5H4.434zM0 15h8v2H0v-2zm0-5h5v2H0v-2z" fill="rgba(24,118,211,1)"/></svg>
                        </span>
                        <input id="email" class="mt-1 " type="email" name="email" value="{{old('email')}}" required placeholder="Email"/>
                    </div>

                    <!-- Phone Number -->
                    <div class="form-e">
                        <span class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M7 2h11a1 1 0 0 1 1 1v18a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V0h2v2zm0 2v5h10V4H7z" fill="rgba(24,118,211,1)"/></svg>
                        </span>
                        <input id="phone" class="mt-1 " type="tel" name="phone" value="{{old('phone')}}" required placeholder="Phone"/>
                    </div>

                    <div class="form-e">
                        <span class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M3 4h18v2H3V4zm0 15h14v2H3v-2zm0-5h18v2H3v-2zm0-5h14v2H3V9z" fill="rgba(24,118,211,1)"/></svg>
                        </span>
                        <input id="title" class="" type="text" name="title" :value="old('title')" required  placeholder="Title"/>
                    </div>
                 
                 
                  
              
                   
                <!-- Role -->
                <!-- <div class="col-span-3">
                    <x-label for="role" :value="__('Role')" />

                    <select class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="role" id="role">
                        <option value="">Select Role</option>
                        <option value="user">User</option>
                        <option value="admin">Administrator</option>
                    </select>
                </div> -->
                <!-- Title -->
         
               
                <!-- Password -->
                <div class="form-e">
                    <span class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="22" height="22"><path fill="none" d="M0 0h24v24H0z"/><path d="M19 10h1a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V11a1 1 0 0 1 1-1h1V9a7 7 0 1 1 14 0v1zM5 12v8h14v-8H5zm6 2h2v4h-2v-4zm6-4V9A5 5 0 0 0 7 9v1h10z" fill="rgba(24,118,211,1)"/></svg>
                    </span>
                    <input  placeholder="Password"  id="password" class="" type="password"  name="password" required autocomplete="new-password" />
                </div>
               
                <!-- Confirm Password -->
                <div class="form-e">
                    <span class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="22" height="22"><path fill="none" d="M0 0h24v24H0z"/><path d="M19 10h1a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V11a1 1 0 0 1 1-1h1V9a7 7 0 1 1 14 0v1zM5 12v8h14v-8H5zm6 2h2v4h-2v-4zm6-4V9A5 5 0 0 0 7 9v1h10z" fill="rgba(24,118,211,1)"/></svg>
                    </span>
                    <input id="password_confirmation" class="" type="password"  name="password_confirmation" required  placeholder="Confirm password"/>
                </div>
                
                    
                <div class="d-flex justify-content-between mt-2">
                    <button class="btn btn-sm btn-primary">
                        {{ __('Register') }}
                    </button>
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                        {{ __('I have an account') }}
                    </a>
                </div>


          
        </form>



        <script type="text/javascript" async defer>
            (function(){
                document.addEventListener("DOMContentLoaded", function(e) { 
                    try {
                        var fileUploaderBtn = document.getElementById("file-upload");
                        var fileReader = new FileReader();
                        //check if fileInput && FileReader exist
                        if(fileReader && fileUploaderBtn) { 
                            //handle change event
                            fileUploaderBtn.addEventListener("change", function(e) { 
                                var previewer = document.getElementById("preview-image-before-upload");
                                fileReader.onload = function (ev) { 
                                    var result = ev.target.result;
                                    previewer.src = ev.target.result;  
                                }
                                fileReader.readAsDataURL(this.files[0]);
                            });
                        }
                    //handler exception error
                    } catch (error) {  console.error(error); }            
                });
            })()
        </script>
