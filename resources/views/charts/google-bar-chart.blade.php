<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js" async defer></script>
<script type="text/javascript" async defer>
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Month', 'Projects'],
      @foreach ($months as $month)
      [ "{{ $month->name }}", {{ $month->projects }} ],
      @endforeach
    ]);

    var options = {
      title: 'Projects per Month'
    };

    var chart = new google.visualization.BarChart(document.getElementById('bar-chart'));

    chart.draw(data, options);
  }
</script>

<div class="container">

	<div id="bar-chart"  style="height: 300px;"></div>

</div>
