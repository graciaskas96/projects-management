
<nav class="left_side">
   <div class="logo_">
        <h3>
          <span class="hilight-orange">Pu.</span> ProjUser
        </h3>
   </div>
   <ul class="menus">
          <!-- Navigation Links -->
          @if(Auth::user()->role == 'admin')
            <div class="">
                <li>
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0 0 172 172"  style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#efefef"><path d="M86,6.88c-43.62608,0 -79.12,35.49048 -79.12,79.12c0,43.62952 35.49392,79.12 79.12,79.12c43.62608,0 79.12,-35.49048 79.12,-79.12c0,-43.62952 -35.49392,-79.12 -79.12,-79.12zM110.46872,36.42616l3.698,-6.16104c0.97696,-1.63056 3.08568,-2.15688 4.71968,-1.17992c1.63056,0.97696 2.15688,3.08912 1.17992,4.71968l-3.698,6.16104c-0.64672,1.07672 -1.78536,1.6684 -2.95152,1.6684c-0.602,0 -1.21432,-0.15824 -1.76816,-0.49192c-1.63056,-0.97352 -2.15688,-3.08568 -1.17992,-4.71624zM82.56,23.7188c0,-1.90232 1.54112,-3.44 3.44,-3.44c1.89888,0 3.44,1.53768 3.44,3.44v7.0864c0,1.90232 -1.54112,3.44 -3.44,3.44c-1.89888,0 -3.44,-1.53768 -3.44,-3.44zM29.44984,52.7524c0.97696,-1.634 3.08912,-2.16032 4.71968,-1.17992l6.16104,3.69456c1.63056,0.97696 2.15688,3.08912 1.17992,4.71968c-0.64672,1.07672 -1.78536,1.6684 -2.95152,1.6684c-0.602,0 -1.21432,-0.15824 -1.76472,-0.49192l-6.16104,-3.69456c-1.634,-0.97352 -2.16376,-3.08568 -1.18336,-4.71624zM20.64,85.6388c0,-1.90232 1.54112,-3.44 3.44,-3.44h7.0864c1.89888,0 3.44,1.53768 3.44,3.44c0,1.90232 -1.54112,3.44 -3.44,3.44h-7.0864c-1.89888,0 -3.44,-1.54112 -3.44,-3.44zM40.33056,116.00368l-6.16104,3.698c-0.55384,0.33368 -1.16272,0.49192 -1.76816,0.49192c-1.1696,0 -2.30824,-0.59512 -2.95152,-1.6684c-0.97696,-1.63056 -0.45064,-3.74272 1.17992,-4.71968l6.16104,-3.698c1.62712,-0.97696 3.74272,-0.45064 4.71968,1.17992c0.97696,1.62368 0.4472,3.73928 -1.17992,4.71624zM60.35136,41.14584c-0.55384,0.33368 -1.16272,0.49192 -1.76816,0.49192c-1.1696,0 -2.30824,-0.59512 -2.95152,-1.6684l-3.698,-6.16104c-0.97696,-1.63056 -0.45064,-3.74272 1.17992,-4.71968c1.63056,-0.97352 3.74272,-0.45064 4.71968,1.17992l3.698,6.16104c0.97696,1.62712 0.45064,3.73928 -1.17992,4.71624zM92.364,94.16312c-1.7372,1.3416 -3.91472,2.15688 -6.364,2.15688c-5.848,0 -10.32,-4.472 -10.32,-10.32c0,-4.64744 2.84488,-8.38328 6.93504,-9.73864l-0.01376,-0.02408l59.53608,-22.9448l-49.75272,40.90848zM142.55016,118.52176c-0.64672,1.07672 -1.78536,1.6684 -2.95152,1.6684c-0.602,0 -1.21432,-0.15824 -1.76816,-0.49192l-6.16104,-3.698c-1.63056,-0.97696 -2.15688,-3.08912 -1.17992,-4.71968c0.97696,-1.63056 3.08912,-2.16032 4.71968,-1.17992l6.16104,3.698c1.63056,0.9804 2.16032,3.09256 1.17992,4.72312zM147.92,89.0788h-7.0864c-1.89888,0 -3.44,-1.53768 -3.44,-3.44c0,-1.90232 1.54112,-3.44 3.44,-3.44h7.0864c1.89888,0 3.44,1.53768 3.44,3.44c0,1.90232 -1.54112,3.44 -3.44,3.44z"></path></g></g></svg>
                    </span>
                    <a href="./"  class="text-white">
                        {{ __('Dashboard') }}
                    </a>
                </li>

                <li>
                    <span>
                        <svg  width="30"  height="30" xmlns="http://www.w3.org/2000/svg"  class="ionicon"  viewBox="0 0 512 512"> <title>People</title><path d="M402 168c-2.93 40.67-33.1 72-66 72s-63.12-31.32-66-72c-3-42.31 26.37-72 66-72s69 30.46 66 72z" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32"/><path d="M336 304c-65.17 0-127.84 32.37-143.54 95.41-2.08 8.34 3.15 16.59 11.72 16.59h263.65c8.57 0 13.77-8.25 11.72-16.59C463.85 335.36 401.18 304 336 304z" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="32"/><path d="M200 185.94c-2.34 32.48-26.72 58.06-53 58.06s-50.7-25.57-53-58.06C91.61 152.15 115.34 128 147 128s55.39 24.77 53 57.94z" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32"/><path d="M206 306c-18.05-8.27-37.93-11.45-59-11.45-52 0-102.1 25.85-114.65 76.2-1.65 6.66 2.53 13.25 9.37 13.25H154" fill="none" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width="32"/></svg>
                    </span>
                    <x-nav-link :href="route('users.index')" :active="request()->routeIs('users.index')" class="text-white">
                        {{ __('Users') }}
                    </x-nav-link>
                </li>


                <li>
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="25" height="25"><path fill="none" d="M0 0h24v24H0z"/><path d="M6.5 2h11a1 1 0 0 1 .8.4L21 6v15a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V6l2.7-3.6a1 1 0 0 1 .8-.4zM19 8H5v12h14V8zm-.5-2L17 4H7L5.5 6h13zM9 10v2a3 3 0 0 0 6 0v-2h2v2a5 5 0 0 1-10 0v-2h2z" fill="rgba(255,255,255,1)"/></svg>
                    </span>
                    <x-nav-link :href="route('projects.index')" :active="request()->routeIs('projects.index')" class="text-white">
                        {{ __('Projects') }}
                    </x-nav-link>
                </li>

                <li>
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"></path><path d="M22 12.999V20a1 1 0 0 1-1 1h-8v-8.001h9zm-11 0V21H3a1 1 0 0 1-1-1v-7.001h9zM11 3v7.999H2V4a1 1 0 0 1 1-1h8zm10 0a1 1 0 0 1 1 1v6.999h-9V3h8z" fill="rgba(239,239,239,1)"></path></svg>
                    </span>
                    <x-nav-link :href="route('categories.index')" :active="request()->routeIs('categories.index')" class="text-white">
                        {{ __('Categories') }}
                    </x-nav-link>
                </li>
               
            </div>
        @else
            <li>
                <span>
                    <svg  width="30"  height="30" xmlns="http://www.w3.org/2000/svg"  class="ionicon"  viewBox="0 0 512 512"> <title>People</title><path d="M402 168c-2.93 40.67-33.1 72-66 72s-63.12-31.32-66-72c-3-42.31 26.37-72 66-72s69 30.46 66 72z" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32"/><path d="M336 304c-65.17 0-127.84 32.37-143.54 95.41-2.08 8.34 3.15 16.59 11.72 16.59h263.65c8.57 0 13.77-8.25 11.72-16.59C463.85 335.36 401.18 304 336 304z" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="32"/><path d="M200 185.94c-2.34 32.48-26.72 58.06-53 58.06s-50.7-25.57-53-58.06C91.61 152.15 115.34 128 147 128s55.39 24.77 53 57.94z" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32"/><path d="M206 306c-18.05-8.27-37.93-11.45-59-11.45-52 0-102.1 25.85-114.65 76.2-1.65 6.66 2.53 13.25 9.37 13.25H154" fill="none" stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width="32"/></svg>
                </span>
                <x-nav-link :href="route('index')" :active="request()->routeIs('index')" class="text-white">
                    {{ __('Home') }}
                </x-nav-link>
            </li>

            <li>
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="25" height="25"><path fill="none" d="M0 0h24v24H0z"/><path d="M6.5 2h11a1 1 0 0 1 .8.4L21 6v15a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V6l2.7-3.6a1 1 0 0 1 .8-.4zM19 8H5v12h14V8zm-.5-2L17 4H7L5.5 6h13zM9 10v2a3 3 0 0 0 6 0v-2h2v2a5 5 0 0 1-10 0v-2h2z" fill="rgba(255,255,255,1)"/></svg>
                </span>
                <x-nav-link :href="route('project.index')" :active="request()->routeIs('project.index')" class="text-white">
                    {{ __('My Projects') }}
                </x-nav-link>
            </li>
        @endif
       
   </ul>

  
    <x-slot name="content">
        <!-- Authentication -->
        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <x-dropdown-link :href="route('logout')"
                    onclick="event.preventDefault();
                                this.closest('form').submit();">
                {{ __('Log Out') }}
            </x-dropdown-link>
        </form>
    </x-slot>

</nav>
