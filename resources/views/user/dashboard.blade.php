<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="container-fluid container_ p-4">
        <div class="row">

            <div class="col-lg-7 bg-white border rounded" style="height:auto !important;">
                @include('charts.google-bar-chart')
            </div>

            <div class="col-lg-5">
                <section style="d-block overflow:auto; height: 400px;">
                    <ul class="p-2">
                       
                        <a href="{{route('project.create')}}" class="btn btn-primary btn-sm">  New project  </a>
                           
                        @foreach($my_projects as $project)
                            <li class=" ">
                                <a href="{{route('project.show', $project->id)}}" class="box__user {{ucfirst($project->status)}}">
                                    <div class="box__image">
                                        <img src="{{Storage::url($project->image_url)}}" alt="" />         
                                    </div>
                                    <div class="box__description">
                                        <p>  {{ucfirst($project->name)}}  </p>
                                        <p>  {{ucfirst($project->title)}} </p>
                                    </div>
                                        
                                       
                                    <div class="col-start-2 row-start-1 row-end-3 sm:mt-4 lg:mt-0 xl:mt-4">
                                        <dt class="sr-only">Users</dt>
                                        <dd x-for="user in project.users" class="flex justify-end sm:justify-start lg:justify-end xl:justify-start -space-x-1.5">
                                            <div class="flex -space-x-1">
                                                <?php
                                                    $assigned_users = DB::table('assign_users')
                                                        ->join('users', 'assign_users.user_id', '=', 'users.id')
                                                        ->where('project_id', '=', $project->id)
                                                        ->select('users.*')->get();
                                                ?>

                                                @foreach($assigned_users as $user)
                                                    <img 
                                                        class="inline-block h-6 w-6 rounded-full ring-2 ring-white" 
                                                        src="{{Storage::url($user->avatar)}}" alt=""
                                                    />
                                                @endforeach

                                            </div>
                                        </dd>
                                    </div>
                                    </dl>
                                </a>
                            </li>
                        @endforeach                      
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
