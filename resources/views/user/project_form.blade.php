<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Project') }}
        </h2>
    </x-slot>

    <div class="container-fluid container_ p-2 border rounded">
        @if(request()->routeIs('project.index'))
            <form class="row" action="{{route('project.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input hidden type="text" name="user_id" value="{{Auth::user()->id}}">
                <div class="col-span-1">
                    <div class="col-span-3 mb-4 w-full space-y-2">
                        <label class="block text-sm font-medium text-gray-700">Name</label>
                        <input class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300" type="text" placeholder="Name..." name="name" />
                    </div>

                            <div class="mb-4 space-y-2">
                                <label for="company-website" class="block text-sm font-medium text-gray-700">
                                    Git Link
                                </label>
                                <div class="mt-1 flex rounded-md shadow-sm">
                                <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                    https://
                                </span>
                                <input type="text" name="link" id="company-website" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300" placeholder="www.example.com">
                                </div>
                            </div>

                            <div class="col-span-2 mb-4 space-y-2">
                                <label class="block text-sm font-medium text-gray-700">Category</label>
                                <div class="space-x-4">
                                    <select name="category" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300">
                                        <option selected disabled>Category...</option>
                                        @foreach($all_categories as $category)
                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="mb-4 space-y-2">
                                <label class="block text-sm font-medium text-gray-700">Description</label>
                                <textarea class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300 resize-none" id="summernote" name="description" cols="30" rows="8" placeholder="Description..."></textarea>
                            </div>

                            <div class="flex-auto flex space-x-4">
                                <button class="btn btn-primary" type="submit">
                                    Create
                                </button>
                                <button class="btn btn-danger" type="reset">
                                    Cancel
                                </button>
                            </div>

                        </div>

                        <div class="col-span-1">
                            <div class="mb-4">
                                <label class="block text-sm font-medium text-gray-700">Image</label>
                                <div class="mt-1 flex justify-center px-6 pt-3 pb-4 border-2 border-gray-300 border-dashed rounded-md">
                                    <div class="space-y-1 text-center">
                                        <svg class="mx-auto h-10 w-10 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                                            <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                        </svg>
                                        <div class="flex text-sm text-gray-600">
                                            <label for="image_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                            <span>upload</span>
                                            <input id="image_upload" name="image_upload" type="file" class="sr-only" accept="image/jpeg, image/png, image/gif">
                                            </label>
                                            <p class="pl-1">or drag and drop</p>
                                        </div>
                                        <p class="text-xs text-gray-500">
                                            PNG, JPG, GIF up to 10MB
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-4">
                                <label class="block text-sm font-medium text-gray-700">Video</label>
                                <div class="mt-1 flex justify-center px-6 pt-3 pb-4 border-2 border-gray-300 border-dashed rounded-md">
                                    <div class="space-y-1 text-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="mx-auto h-10 w-10 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M17 14v6m-3-3h6M6 10h2a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2zm10 0h2a2 2 0 002-2V6a2 2 0 00-2-2h-2a2 2 0 00-2 2v2a2 2 0 002 2zM6 20h2a2 2 0 002-2v-2a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2z" />
                                        </svg>
                                        <div class="flex text-sm text-gray-600">
                                            <label for="video_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                            <span>upload</span>
                                            <input id="video_upload" name="video_upload" type="file" class="sr-only" accept="video/*">
                                            </label>
                                            <p class="pl-1">or drag and drop</p>
                                        </div>
                                        <p class="text-xs text-gray-500">
                                            MPG, MP4, AVI up to 128MB
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-4">
                                <label class="block text-sm font-medium text-gray-700">Document</label>
                                <div class="mt-1 flex justify-center px-6 pt-3 pb-4 border-2 border-gray-300 border-dashed rounded-md">
                                    <div class="space-y-1 text-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="mx-auto h-10 w-10 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                                        </svg>
                                        <div class="flex text-sm text-gray-600">
                                            <label for="document_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                            <span>upload</span>
                                            <input id="document_upload" name="document_upload" type="file" class="sr-only" accept="application/pdf, application/vnd.ms-excel">
                                            </label>
                                            <p class="pl-1">or drag and drop</p>
                                        </div>
                                        <p class="text-xs text-gray-500">
                                            TXT, DOC, XLS up to 10MB
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                @else


                    <form class="row bg-white p-4" action="{{route('project.update', $edit_project->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <input hidden type="text" name="user_id" value="{{Auth::user()->id}}">
                        
                        <div class="col-lg-7">
                            <div class="form-input">
                                <label class="text-primary">Name</label>
                                <input class="form-control" type="text" placeholder="Name..." name="name" value="{{$edit_project->name}}" />
                            </div>

                            <div class="form-input">
                                <label for="company-website" class="block text-sm font-medium text-gray-700">  Git Link
                                  
                                </label>
                                <input  type="text"  name="link"  id="company-website"  class="form-control" placeholder="www.example.com"  value="{{$edit_project->link}}"/>
                            </div>

                            <div class="form-input">
                                <label class="">Category</label>
                                <div class="form-input">
                                    <select name="category" class="form-control">
                                        <option selected disabled>Category...</option>
                                        @foreach($all_categories as $category)
                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-input">
                                <label class="block text-sm font-medium text-gray-700">Description</label>
                                <textarea class="form-control" id="summernote" name="description" cols="30" rows="8" placeholder="Description...">
                                    {{$edit_project->description}}
                                </textarea>
                            </div>

                            <div class="flex-auto flex space-x-4">
                                <button class="btn btn-primary" type="submit">
                                    Update
                                </button>
                                <button class="btn btn-danger" type="reset">
                                    Reset
                                </button>
                            </div>
                        </div>

                

                        <div class="col-lg-1"></div>
                        <div class="col-lg-4">

                            <div class="row text-primary">
                                <div class="col-lg-6 mb-2 ">
                                    <label class="d-block">Image</label>
                                    <div class="d-flex justify-content-center p-2 border rounded">
                                        <div class="space-y-1 text-center">
                                            <svg stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true" width="50px">
                                                <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            <div class="">
                                                <label for="image_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    <span class="btn btn-primary btn-sm">upload</span>
                                                    <input id="image_upload" name="image_upload" type="file" class="sr-only" accept="image/jpeg, image/png, image/gif">
                                                </label>
                                            </div>
                                            <p class="text-xs text-gray-500"> PNG, JPG, GIF up to 10MB  </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 mb-2 text-primary">
                                    <label class="d-block">Video</label>
                                    <div class="d-flex justify-content-center p-2 border rounded">
                                        <div class="space-y-1 text-center">
                                            <svg  width="50px" xmlns="http://www.w3.org/2000/svg" class="mx-auto h-10 w-10 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M17 14v6m-3-3h6M6 10h2a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2zm10 0h2a2 2 0 002-2V6a2 2 0 00-2-2h-2a2 2 0 00-2 2v2a2 2 0 002 2zM6 20h2a2 2 0 002-2v-2a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2z" />
                                            </svg>
                                            <div class="flex text-sm text-gray-600">
                                                <label for="video_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    <span class="btn btn-primary btn-sm">upload</span>
                                                    <input id="video_upload" name="video_upload" type="file" class="sr-only" accept="video/*">
                                                </label>
                                            </div>
                                            <p class="text-xs text-gray-500">MPG, MP4, AVI up to 128MB  </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            
                            

                            <div class="col-lg-6 text-primary">
                                <label class="d-block">Document</label>
                                <div class="d-flex justify-content-center p-2 border rounded">
                                    <div class="space-y-1 text-center">
                                        <svg  width="50px" xmlns="http://www.w3.org/2000/svg" class="mx-auto h-10 w-10 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                                        </svg>
                                        <div class="flex text-sm text-gray-600">
                                            <label for="document_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                            <span class="btn btn-primary btn-sm">upload</span>
                                            <input id="document_upload" name="document_upload" type="file" class="sr-only" accept="application/pdf, application/vnd.ms-excel">
                                            </label>
                                            
                                        </div>
                                        <p class="text-xs text-gray-500">
                                            TXT, DOC, XLS up to 10MB
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                @endif        
            </div>
        </div>
    </div>

</x-app-layout>
