<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects') }}
        </h2>
    </x-slot>


    <div class="p-2">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <section class="relative bg-blueGray-50">
                <div class="w-full mb-12">
                    <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded  bg-blue-500 text-white">
                        <div class="bg-white">
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th class="">Project</th>
                                        <th class="">Status</th>
                                        <th class="">Members</th>
                                        <th class="">Completion </th>
                                        <th class="">Modification</th>
                                    </tr>
                                </thead>

                                <tbody class="divide-y">
                                    @foreach($my_projects as $project)
                                        <tr>
                                            <td class="">
                                                @if($project->image_url == 'N/A')
                                                    <small class="rounded border">
                                                        no image
                                                    </small>
                                                @else
                                                    <img src="{{Storage::url($project->image_url)}}" class="rounded border" alt="...">
                                                @endif

                                                {{$project->name}} 
                                            </td>
                                            </td>
                                            <td class="">
                                                <i class="badge badge-success"></i>{{$project->status}}</td>
                                            <td class="">
                                                <div class="flex">
                                                <?php
                                                    $assigned_users = DB::table('assign_users')
                                                        ->join('users', 'assign_users.user_id', '=', 'users.id')
                                                        ->where('project_id', '=', $project->id)
                                                        ->select('users.*')->get();
                                                ?>

                                                @foreach($assigned_users as $user)
                                                    <img src="{{Storage::url($user->avatar)}}" alt="..." class="w-6 h-6 rounded-full border-2 border-blueGray-50 shadow -ml-4">
                                                @endforeach
                                                </div>
                                            </td>

                                            <td class="">
                                               60%
                                            </td>

                                            <td class="">
                                                <a href="{{route('project.edit', $project->id)}}" class="text-white bg-primary status">Edit</a>
                                                <a href="{{route('project.destroy', $project->id)}}" class="bg-danger text-white status">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </section>

        </div>
    </div>
</x-app-layout>
