<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects') }}
        </h2>
    </x-slot>


    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="md:grid md:grid-cols-3 md:gap-6">
                <div class="md:col-span-1">
                    <div class="px-4 sm:px-0">
                        <div class="container mx-auto my-24">
                            <div class="rounded-md bg-white relative shadow mx-auto">
                                <div class="flex justify-center">
                                    @if($edit_user->avatar == 'N/A')
                                        <span class="flex justify-center items-center rounded-full mx-auto absolute -top-24 w-36 h-36 shadow-2xl border-4 border-white bg-white">no image</span>
                                    @else
                                        <img src="{{Storage::url($edit_user->avatar)}}" alt="" class="rounded-full mx-auto absolute -top-24 w-36 h-36 shadow-2xl border-4 border-white">
                                    @endif
                                </div>

                                <div class="mt-16">
                                    <h1 class="font-bold text-center text-3xl text-gray-900">{{$edit_user->name}}</h1>
                                    <p class="text-center text-sm text-gray-400 font-medium">{{$edit_user->title}}</p>

                                    <div class="w-full">
                                        <div class="mt-5 w-full">
                                            <a href="#" class="w-full border-t-2 border-gray-100 font-medium text-gray-600 py-4 px-4 block hover:bg-gray-100 transition duration-150">
                                                Email
                                                <span class="text-gray-400 text-sm">{{$edit_user->email}}</span>
                                            </a>

                                            <a href="#" class="w-full border-t-2 border-gray-100 font-medium text-gray-600 py-4 px-4 block hover:bg-gray-100 transition duration-150">
                                                Phone
                                                <span class="text-gray-400 text-sm">{{$edit_user->phone}}</span>
                                            </a>

                                            <a href="#" class="w-full border-t-2 border-gray-100 font-medium text-gray-600 py-4 px-4 block hover:bg-gray-100 transition duration-150">
                                                Role
                                                <span class="text-gray-400 text-sm">{{$edit_user->role}}</span>
                                            </a>

                                            <a href="#" class="w-full border-t-2 border-gray-100 font-medium text-gray-600 py-4 px-4 block hover:bg-gray-100 transition duration-150">
                                                Gender
                                                <span class="text-gray-400 text-sm">{{$edit_user->gender}}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-5 md:mt-0 md:col-span-2">
                    <form action="{{route('users.update', $edit_user->id)}}" method="POST">
                        @csrf
                        @method('put')
                        <div class="shadow rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div>
                                    <label class="block text-sm font-medium text-gray-700">
                                        Photo
                                    </label>
                                    <div class="mt-2 flex items-center">
                                        <span class="inline-block h-12 w-12 rounded-full overflow-hidden bg-gray-100">
                                            <svg class="h-full w-full text-gray-300" fill="currentColor" viewBox="0 0 24 24">
                                                <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
                                            </svg>
                                        </span>
                                        <label for="file-upload" class="relative cursor-pointer bg-white font-medium focus-within:outline-none">
                                            <span class="ml-5 bg-white py-2 px-3 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Change</span>
                                            <input id="file-upload" name="avatar" type="file" value="{{Storage::url($edit_user->avatar)}}" class="sr-only">
                                        </label>
                                    </div>
                                </div>

                                <div class="grid grid-cols-3 gap-6">

                                </div>

                                <div class="grid grid-cols-6 gap-6">
                                    <div class="col-span-3">
                                        <label for="name" class="block text-sm font-medium text-gray-700">Name</label>
                                        <input type="text" name="name" id="name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{$edit_user->name}}" placeholder="Name">
                                    </div>

                                    <div class="col-span-6 sm:col-span-3">
                                        <label for="email" class="block text-sm font-medium text-gray-700">Email</label>
                                        <input type="text" name="email" id="email" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{$edit_user->email}}" placeholder="Email">
                                    </div>
                                </div>

                                <div class="grid grid-cols-6 gap-6">
                                    <div class="col-span-3">
                                        <label for="gender" class="block text-sm font-medium text-gray-700">Gender</label>
                                        <select id="gender" name="gender" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                            <option selected disable>Select Gender</option>
                                            @if($edit_user->gender == "M")
                                                <option selected value="M">Male</option>
                                                <option value="F">Female</option>
                                            @elseif($edit_user->gender == "F")
                                                <option value="M">Male</option>
                                                <option selected value="F">Female</option>
                                            @else
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            @endif
                                        </select>
                                    </div>

                                    <div class="col-span-6 sm:col-span-3">
                                        <label for="phone" class="block text-sm font-medium text-gray-700">Phone</label>
                                        <input type="text" name="phone" id="phone" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{$edit_user->phone}}" placeholder="Phone">
                                    </div>
                                </div>

                                <div class="grid grid-cols-6 gap-6">
                                    <div class="col-span-3">
                                        <label for="title" class="block text-sm font-medium text-gray-700">Title</label>
                                        <input type="tel" name="title" id="title" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{$edit_user->title}}" placeholder="Title">
                                    </div>

                                    <div class="col-span-3">
                                        <label for="role" class="block text-sm font-medium text-gray-700">Role</label>
                                        <select id="role" name="role" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                            <option selected disable>Select Role</option>
                                            @if($edit_user->role == "admin")
                                                <option selected value="admin">Administrator</option>
                                                <option value="user">User</option>
                                            @elseif($edit_user->role == "user")
                                                <option value="admin">Administrator</option>
                                                <option selected value="user">User</option>
                                            @else
                                                <option value="admin">Administrator</option>
                                                <option value="user">User</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="grid grid-cols-6 gap-6">
                                    <div class="col-span-3">
                                        <label for="password" class="block text-sm font-medium text-gray-700">Password</label>
                                        <input type="password" name="password" id="password" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{$edit_user->password}}" placeholder="Password">
                                    </div>

                                    <div class="col-span-6 sm:col-span-3">
                                        <label for="password_confirmation" class="block text-sm font-medium text-gray-700">Confirm Password</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" placeholder="Confirm Password">
                                    </div>
                                </div> -->
                            </div>
                            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-500 hover:bg-yellow-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    Cancel
                                </button>

                                <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-500 hover:bg-yellow-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
