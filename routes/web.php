<?php

use App\Http\Controllers\AdminHomeController;
use App\Http\Controllers\AssignUserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    // dashboard routes
    Route::resource('/', AdminHomeController::class)->name('*', 'dashboard');

    // projects routes
    Route::resource('/projects', ProjectController::class)->name('*', 'projects');

    // projects category routes
    Route::resource('/categories', CategoryController::class)->name('*', 'categories');

    // users routes
    Route::resource('/users', UserController::class)->name('*', 'users');

    // profile routes
    Route::get('/profile', function () {return view('welcome');})->name('Profile');
});

Route::group(['prefix' => '', 'middleware' => ['auth', 'user']], function () {

    // Route::get('/', function () {
    //     return view('user.dashboard');
    // })->middleware(['auth'])->name('dashboard');

    // dashboard routes
    Route::resource('/', HomeController::class)->name('*', 'dashboard');

    // user routes
    Route::resource('/user', UserController::class)->name('*', 'user');

    // projects routes
    Route::resource('/my/project', ProjectController::class)->name('*', 'project');

    // projects category routes
    Route::resource('/assign', AssignUserController::class)->middleware(['auth'])->name('*', 'assign');
});

require __DIR__ . '/auth.php';
